import AuthController from "./authentication-controller";

const routes = [{
    path: "/register",
    method: "POST",
    config: {
      auth: false
    },
    handler: AuthController.register
  },

  {
    path: "/login",
    method: "POST",
    config: {
      auth: false
    },
    handler: AuthController.authorize
  }
];

export default routes;