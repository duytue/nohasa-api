import ProductController from "./product-controller";

const routes = [{
    path: "/getAllProduct",
    method: "GET",
    config: {
      auth: false
    },
    handler: ProductController.getAllProduct
  },
  {
    path: "/getAllProductByAltName",
    method: "GET",
    config: {
      auth: false
    },
    handler: ProductController.getAllProductByAltName
  },
  {
    path: "/addProduct",
    method: "POST",
    config: {
      auth: "jwt"
    },
    handler: ProductController.addProduct
  },
  {
    path: "/addImageToProduct",
    method: "POST",
    config: {
      auth: "jwt"
    },
    handler: ProductController.addImageToProduct
  },
  {
    path: "/getProductImageById",
    method: "GET",
    config: {
      auth: "jwt"
    },
    handler: ProductController.getProductImageById
  },
  {
    path: "/updateProductDefaultImage",
    method: "POST",
    config: {
      auth: "jwt"
    },
    handler: ProductController.updateProductDefaultImage
  },
  {
    path: "/getProductsForAdmin",
    method: "GET",
    config: {
      auth: false
    },
    handler: ProductController.getAllProductForAdminPage
  },
  {
    path: "/deleteProduct",
    method: "GET",
    config: {
      auth: false
    },
    handler: ProductController.deleteProduct
  },
  {
    path: "/updateProduct",
    method: "PUT",
    config: {
      auth: false
    },
    handler: ProductController.updateProduct
  },
  {
    path: "/search",
    method: "GET",
    config: {
      auth: false
    },
    handler: ProductController.searchProducts
  }
];

export default routes;