import Bookshelf from "../database";

const ProductImages = Bookshelf.Model.extend({
  tableName: "productimages"
});

export default ProductImages;