import UserController from "./user-controller";

const routes = [{
  path: "/getUserInfoByUserName",
  method: "GET",
  config: {
    auth: "jwt"
  },
  handler: UserController.getUserInfoByUserName
}];

export default routes;