import Boom from "boom";
import Bookshelf from "../../db/database";

class UserController {
  constructor() {}

  getUserInfoByUserName(request, reply) {
    const {
      username
    } = request.query;

    const rawSql = `select username, 
                          name, 
                          email, 
                          phone,
                          birthday,
                          gender
                    from accounts where username = "${username}"`;

    Bookshelf.knex.raw(rawSql)
      .then((result) => {
        if (result !== null) {
          reply({
            data: result[0]
          });
          return;
        }
      })
      .catch((error) => {
        reply(Boom.badRequest(error));
        return;
      });
  }

}

export default new UserController();