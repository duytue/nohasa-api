import Bookshelf from "../database";

const Products = Bookshelf.Model.extend({
    tableName: "products"
});

export default Products;