import Boom from "boom";
import Bookshelf from "../../db/database";
import Products from "../../db/model/products";
import ProductImages from "../../db/model/productImages";
const logger = require("../../helper/logger");

class ProductController {
    constructor() {}

    getAllProduct(request, reply) {

        const rawSql = `select * from products p, productimages i where p.id = i.productId and (isDeleted is null or isDeleted = 0)`;

        Bookshelf.knex.raw(rawSql)
            .then((result) => {
                if (result !== null) {
                    reply({
                        data: result[0]
                    });
                    return;
                }
            })
            .catch((error) => {
                reply(Boom.badRequest(error));
                return;
            });
    }

    searchProducts(request, reply) {

        const {
            searchQuery
        } = request.query;

        if (searchQuery !== null || searchQuery !== undefined) {

            const rawSql = `select * from products p, productimages i where p.name like '%${searchQuery}%' and p.id = i.productId and (isDeleted is null or isDeleted = 0) `;

            Bookshelf.knex.raw(rawSql)
                .then((result) => {
                    if (result !== null) {
                        reply({
                            data: result[0]
                        });
                        return;
                    }
                })
                .catch((error) => {
                    reply(Boom.badRequest(error));
                    return;
                });
        } else {
            return;
        }
    }

    getAllProductByAltName(request, reply) {

        const {
            altName
        } = request.query;

        const rawSql = `select * from products p, productimages i where p.id = i.productId and altName = "${altName}" and (isDeleted is null or isDeleted = 0)`;

        Bookshelf.knex.raw(rawSql)
            .then((result) => {
                if (result !== null) {
                    reply({
                        data: {
                            singleProduct: result[0][0],
                            imageData: result[0]
                        }
                    });
                    return;
                }
            })
            .catch((error) => {
                reply(Boom.badRequest(error));
                return;
            });
    }

    addProduct(request, reply) {

        const {
            name,
            description,
            price,
            altName
        } = request.payload;

        new Products().save({
                Name: name,
                Description: description,
                Price: price,
                AltName: altName
            }, {
                method: "insert"
            })
            .then(result => {
                if (result === null) {
                    reply(Boom.badRequest("Unable to add product"));
                    return;
                }

                reply({
                    productId: result.id,
                    message: "Product added successfully"
                });
            })
            .catch(error => {
                reply(Boom.badRequest("Unable to add product"));
                logger.error("addProduct: " + error);
            });
    }

    addImageToProduct(request, reply) {
        const {
            uploadName,
            imageURL,
            productId
        } = request.payload;

        new ProductImages().save({
                UploadName: uploadName,
                ImageURL: imageURL,
                ProductId: productId
            }, {
                method: "insert"
            })
            .then(result => {
                if (result === null) {
                    reply(Boom.badRequest("Unable to add image(s) to product"));
                    return;
                }

                reply({
                    imageId: result.id,
                    message: "Product image(s) added successfully"
                });
            })
            .catch(error => {
                reply(Boom.badRequest("Unable to add image(s) to product"));
                logger.error("addImageToProduct: " + error);
            });
    }

    getProductImageById(request, reply) {
        const {
            productId
        } = request.query;

        const rawSql = `select * from productimages where productId = "${productId}"`;

        Bookshelf.knex.raw(rawSql)
            .then((result) => {
                if (result !== null) {
                    reply({
                        data: result[0]
                    });
                    return;
                }
            })
            .catch((error) => {
                reply(Boom.badRequest(error));
                return;
            });
    }

    updateProductDefaultImage(request, reply) {
        const {
            productId,
            isHomePageImg,
            imageId
        } = request.payload;

        const rawSql = `update productimages set isHomePageImg = ${isHomePageImg} 
                                            where productId = ${productId} 
                                            and id = ${imageId}`;

        const rawSql2 = `update productimages set isHomePageImg = 0 where productId = ${productId} and id not in (${imageId})`;

        Bookshelf.knex.raw(rawSql)
            .then(() => {

                Bookshelf.knex.raw(rawSql2)
                    .then((result) => {
                        if (result !== null) {
                            reply({
                                message: "Update product's default image successfully",
                                isSuccess: true
                            });
                            return;
                        }
                    })
                    .catch((error) => {
                        reply({
                            message: Boom.badRequest(error),
                            isSuccess: false
                        });
                        return;
                    });
            })
            .catch((error) => {
                reply({
                    message: Boom.badRequest(error),
                    isSuccess: false
                });
                return;
            });


    }

    getAllProductForAdminPage(request, reply) {

        const rawSql = `select * from products`;

        Bookshelf.knex.raw(rawSql)
            .then((result) => {
                if (result !== null) {
                    reply({
                        data: result[0]
                    });
                    return;
                }
            })
            .catch((error) => {
                reply(Boom.badRequest(error));
                return;
            });
    }

    deleteProduct(request, reply) {

        const {
            productId
        } = request.query;

        const rawSql = `update products set isDeleted = 1 where id = ${productId}`;

        Bookshelf.knex.raw(rawSql)
            .then((result) => {
                if (result !== null) {
                    reply({
                        isSuccess: true
                    });
                    return;
                }
            })
            .catch((error) => {
                reply(Boom.badRequest(error));
                return;
            });
    }

    updateProduct(request, reply) {

        const {
            productId,
            name,
            description,
            price,
            altName,
            isDeleted
        } = request.payload;

        const rawSql = `update products set name="${name}", description="${description}", price=${price}, altName="${altName}", isDeleted=${isDeleted} where id=${productId}`;

        Bookshelf.knex.raw(rawSql)
            .then((result) => {
                if (result !== null) {
                    reply({
                        isSuccess: true
                    });
                    return;
                }
            })
            .catch((error) => {
                reply(Boom.badRequest(error));
                return;
            });
    }
}

export default new ProductController();