drop database if exists nohasa;
create database nohasa;
use nohasa;

CREATE TABLE `products` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` nvarchar(1024) NOT NULL,
	`description` nvarchar(3000) NOT NULL,
	`img` nvarchar(3000) NOT NULL,
	`price` FLOAT NOT NULL,
	PRIMARY KEY (`id`)
);
